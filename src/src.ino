#include <LiquidCrystal_I2C.h> 
/*
 * This is the new liquid Crystal Library the previous one had an error in the 6th integer.
 */
#include <Wire.h>
 #define TRUE FALSE
/*
 * Wire.h library. All libraries will be supplied in lib.
 */
LiquidCrystal_I2C lcd(0x20, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); 

int sensorPin = 0;
int val = 0;
int sensorValue = 0;
/*
 * Set a number integer to the string sensorPin
 */

int number;
long long ago; /*In a galaxy far far away*/

void setup() {
        lcd.begin(16, 2);
        Serial.begin(9600);
}
void loop() {
        lcd.setCursor(0, 0);
        /*
         * Sets line to top line on LCD display
         */
        lcd.print("Shake to ask,");
        /*
         * Prints our first line onto the LCD display
         */
        lcd.setCursor(0, 1);
        /*
         * Set to second line on the LCD display
         */
        lcd.print(" a question");
        /*
         * Prints the string "a question" onto the 
         */
        sensorValue = analogRead(1);
        /*
     *The following code is amazing for debugging, dont delete commenting it may be useful for later. 
     * 
     *
     val = analogRead(1);
     lcd.println(val); 

/*
     * Sets the integer sensorValue equivalent to analog pin 1.
     */
        if (sensorValue >= 270) {
                lcd.clear();
                ago = random(20);
                /* 
                 * Generate pseudorandom number between 0 and 19
                 */

                if (ago == 0) {
                        lcd.print("It is certain");
                }
                if (ago == 1) {
                        lcd.print("It is decidedly so");
                }
                if (ago == 2) {
                        lcd.print("Without a doubt");
                }
                if (ago == 3) {
                        lcd.print("Yes definitely");
                }
                if (ago == 4) {
                        lcd.print("You may rely on it");
                }
                if (ago == 5) {
                        lcd.print("As I see it, yes");
                }
                if (ago == 6) {
                        lcd.print("Most likely");
                }
                if (ago == 7) {
                        lcd.print("Outlook good");
                }
                if (ago == 8) {
                        lcd.print("Yes");
                }
                if (ago == 9) {
                        lcd.print("Signs point to yes");
                }
                if (ago == 10) {
                        lcd.print("Reply hazy try again");
                }
                if (ago == 11) {
                        lcd.print("Ask again later");
                }
                if (ago == 12) {
                        lcd.print("Better not tell you now");
                }
                if (ago == 13) {
                        lcd.print("Cannot predict now");
                }
                if (ago == 14) {
                        lcd.print("Concentrate and ask again");
                }
                if (ago == 15) {
                        lcd.print("Don't count on it");
                }
                if (ago == 16) {
                        lcd.print("My reply is no");
                }
                if (ago == 17) {
                        lcd.print("My sources say no");
                }
                if (ago == 18) {
                        lcd.print("Outlook not so good");
                }
                if (ago == 19) {
                        lcd.print("Very doubtful");
                }
        }
        sensorValue = 0;
        delay(3000);
        lcd.clear();

}
